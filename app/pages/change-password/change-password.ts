import { Component } from '@angular/core';
import { NavController, MenuController, Alert, Loading} from 'ionic-angular';
import {FORM_DIRECTIVES, FormBuilder, Control, ControlGroup, Validators, AbstractControl} from '@angular/common';
import {AuthService, CurrentUserService} from "../../shared"

interface IFormModel {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
}

interface ValidationResult {
  [key: string]: boolean;
}

@Component({
  templateUrl: 'build/pages/change-password/change-password.html',
})
export class ChangePasswordPage {
  passwordDoesntMatch: boolean = false;
  formModel: IFormModel = {
    oldPassword: "",
    newPassword: "",
    confirmPassword: ""
  };
  constructor(
    private nav: NavController,
    private authService: AuthService,
    private currentUserService: CurrentUserService,
    private menu: MenuController) {
  }
  areEqual(group: ControlGroup) {
    var valid = false;

    if (group.controls['newPassword'].value === group.controls['confirmPassword'].value) {
      return null;
    }

    return {
      areEqual: true
    };
  }

  confirmChangePassword(changePasswordForm: ControlGroup) {
    if (!changePasswordForm.valid) {
      return;
    } else if (!!this.areEqual(changePasswordForm)) {
      this.passwordDoesntMatch = true;
      return;
    }

    this.passwordDoesntMatch = false;

    let confirm = Alert.create({
      title: 'Reset Password Confirmation',
      message: 'Are you sure you want to rest your password? ',
      buttons: ['Cancel',
        {
          text: 'Ok',
          handler: () => {
            confirm.dismiss();
            return this.handleChangePassword();
          }
        }
      ]
    });
    this.nav.present(confirm);
  }

  handleChangePassword() {
    let updatePasswordLoading = Loading
      .create({
        content: '<ion-spinner></ion-spinner><div>Updating Your Password</div>',
        // showBackdrop: true
      });
    this.nav.present(updatePasswordLoading);
    return this.authService
      .changeUserPassword(this.formModel.oldPassword, this.formModel.newPassword, this.formModel.confirmPassword)
      .subscribe((data) => {
        updatePasswordLoading.dismiss();
        if (data != 1 && data.success == 0) {
          let wrongPasswordAlert = Alert.create({
            title: "Change Password",
            message: "Old Password Isn't Correct !",
            buttons: ['OK']
          });
          this.nav.present(wrongPasswordAlert);
          return data.message;
        }
        else {
          var userData = this.currentUserService.getCurrentUserData();
          userData.dm_changePassword = 0;
          this.currentUserService.setCurrentUserData(userData);
          let sucessAlert = Alert.create({
            title: "Password Updated",
            message: "You Password has been updated successfully",
            buttons: [{
              text: "OK",
              handler: () => {
                sucessAlert.dismiss();
                this.menu.open();
              }
            }]
          });
          this.nav.present(sucessAlert);
        }

      });
  }

}
