import {Page, NavController, Alert, Loading} from 'ionic-angular';
import {Injectable} from '@angular/core';

import {ENV, ApiUrlsService, AuthService, CurrentUserService} from '../../shared';
import {LoginModel} from './shared';
import {ShiftsTabsPage} from '../shifts/shifts-tabs';
/*
  Generated class for the LoginPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Injectable()
@Page({
  templateUrl: 'build/pages/login/login.html',
  providers: [ApiUrlsService, ENV, CurrentUserService, AuthService]
})
export class LoginPage {

  loginData: { username?: string, password?: string, subDomain?: string } = {};
  subDomains = [];
  showLoadingSubDomainsSpinner = false;
  loggingYouInSpinner = false;
  userNotRegistered = false;
  loading: Loading;

  constructor(
    public nav: NavController,
    private apiUrlsService: ApiUrlsService,
    private authService: AuthService) {
    this.loginData = {
      username: "",
      password: "",
      subDomain: ""
    };///////
  }
  ngOnInit() {
  }

  doLogin(loginForm) {
    if (!loginForm.valid) return;
    let loginDataModel: LoginModel = {
      password: this.loginData.password,
      userName: this.loginData.username
    };
    this.apiUrlsService.setDbName(this.loginData.subDomain);
    this.loggingYouInSpinner = true;
    this.authService
      .login(loginDataModel)
      .then((userData) => {
        this.loggingYouInSpinner = false;
      })
      .catch(() => {
        let failedAlert = Alert.create({
          title: "login Failed",
          message: " Invalid UserName Or Password",
          buttons: ['OK']
        });
        this.nav.present(failedAlert);
        this.loggingYouInSpinner = false;
      });
  }

  loadSubdomains() {

    this.showLoadingSubDomainsSpinner = true;

    this.apiUrlsService
      .getUserSubDomainsByEmail(this.loginData.username)
      .subscribe(subdomains => {
        this.subDomains = subdomains;
        if (!this.subDomains || !this.subDomains.length) {
          this.userNotRegistered = true;
        } else {
          this.loginData.subDomain = subdomains[0].subDomain;
          this.userNotRegistered = false;
        }
        this.showLoadingSubDomainsSpinner = false;
      }, (err) => {
        this.userNotRegistered = true;
        this.showLoadingSubDomainsSpinner = false;
      });
  }

  confirmResetPassword(ev) {
    ev.preventDefault();
    let confirm = Alert.create({
      title: 'Reset Password Confirmation',
      message: 'Are you sure you want to rest your password? ',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
            this.sendResetPasswordRequest();
          }
        }
      ]
    });
    this.nav.present(confirm);
  }

  sendResetPasswordRequest() {
    //show loading spinner with login in message
    if (!this.loginData.username) {
      let emailIsRequiredAlert = Alert.create({
        title: 'Email is Required',
        message: "Please Enter A Valid Email Address.",
        buttons: ['OK']
      });
      this.nav.present(emailIsRequiredAlert);
      return;
    }

    let passwordResetLoading = Loading
      .create({
        content: '<ion-spinner></ion-spinner><div>Sending Password Renewal Request</div>',
        showBackdrop: true
      });
    this.nav.present(passwordResetLoading);

    this.apiUrlsService
      .getUserSubDomainsByEmail(this.loginData.username)
      .subscribe(subdomains => {
        this.subDomains = subdomains;
        this.showLoadingSubDomainsSpinner = false;
        if (!this.subDomains || !this.subDomains.length) {
          this.userNotRegistered = true;

          passwordResetLoading.dismiss();
          let resetResult = Alert.create({
            title: 'Email/username is not Registered ',
            message: "This Email/username is not registered yet ",
            buttons: ['OK']
          });
          this.nav.present(resetResult);
        } else {

          return this.authService
            .sendResetPasswordRequest(this.loginData.username)
            .then((data) => {
              passwordResetLoading.dismiss();
              setTimeout(() => {
                let test = Alert.create({
                  title: 'Password Renewal Request',
                  message: "Password Reset Steps Have been Sent to You, Please Check Your email.",
                  buttons: ['OK']
                });

                this.nav.present(test);

              }, 100);

            });
        }

      }, (err) => {
        this.userNotRegistered = true;
        this.showLoadingSubDomainsSpinner = false;
      });
  }

}
