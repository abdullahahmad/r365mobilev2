import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import { Observable } from 'rxjs/Observable';

import {ApiUrlsService} from '../../../shared/';
import {LoginModel} from './';

@Injectable()
export class LoginService {
    subDomains = [];
    constructor(private http:Http, private apiUrlsService : ApiUrlsService ) { 
        
        (<any>window).$http = this.http;
    }
    
    login(loginModel:LoginModel){
        let apiUrl =  "";
        let payLoadDate  = JSON.stringify(loginModel);
       
       return this.http
            .post(apiUrl,payLoadDate)
            .toPromise()
            .then((response) =>{
                return response.json();
            });
    }

}