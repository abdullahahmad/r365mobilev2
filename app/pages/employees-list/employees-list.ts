import {Page, NavController, Loading} from 'ionic-angular';
import {OnInit} from "@angular/core";
import {EmployeesListService} from "./"
/*
  Generated class for the EmployeesListPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/employees-list/employees-list.html',
  providers: [EmployeesListService]
})
export class EmployeesListPage implements OnInit {
  employeesList = [];
  employeesListCopy = [];
  employeesLoading: Loading;
  searchTerm = "";
  constructor(public nav: NavController, private employeesService: EmployeesListService) { }

  ngOnInit() {
    this.getEmployeesList();
  }

  filterItems(event) {
    let keyword = this.searchTerm.toLowerCase();
    if (keyword && keyword.length > 0) {
      this.employeesList = this.employeesListCopy.filter((item) => {
        return (item.FullName.toLowerCase().indexOf(keyword) >= 0)
        // return item;
      });

    } else {
      Object.assign(this.employeesList, this.employeesListCopy);
    }
  }

  goToEmployeePage(employee) {
    // this.nav.push(EmployeeFormPage, { Id: employee.Id })
  }

  getEmployeesList(refresher?: any) {
    let forceRefresh = refresher ? true : false;
    this.employeesLoading = Loading.create({
      content: "Loading Employees",
      showBackdrop: true
    });
    this.nav.present(this.employeesLoading);

    this.employeesService
      .getEmployeesList(forceRefresh)
      .subscribe((employeeList) => {
        this.employeesList = employeeList;
        Object.assign(this.employeesListCopy, employeeList);
        this.employeesLoading.dismiss();
        if (refresher) {
          refresher.complete();
        }
      }, (error) => {
        alert("UnExpectedError! failed to get employees list")
        this.employeesLoading.dismiss();
      });
  }

  doRefresh(refresher) {
    this.getEmployeesList(refresher);
  }

  sendEmail(emailAddress) {

    if (emailAddress) {
      window.location.href = "mailto:" + emailAddress;
    }
    // $ionicListDelegate.closeOptionButtons();
  }

  callPhone(phoneNumber) {

    if (phoneNumber) {
      window.location.href = "tel:" + phoneNumber;
    }
    // $ionicListDelegate.closeOptionButtons();
  }

  sendSmsMsg(phoneNumber, evnet) {
    console.log(event);


    if (phoneNumber) {
      window.location.href = "sms:" + phoneNumber;
    }
    // $ionicListDelegate.closeOptionButtons();
  }
}
