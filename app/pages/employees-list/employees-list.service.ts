import { Injectable } from '@angular/core';
import {Http, Response, RequestOptionsArgs, Headers, URLSearchParams} from "@angular/http";
import { Observable } from 'rxjs/Observable';

import {ApiUrlsService, CurrentUserService, R365GUID} from '../../shared/';

@Injectable()
export class EmployeesListService {

    employeesList: any[] = [];

    constructor(private apiUrlsService: ApiUrlsService, private http: Http, private currentUserService: CurrentUserService) {

    }

    getEmployeesList(forceRelaod?: boolean) {


        if (this.employeesList.length && !forceRelaod) {

            let observable = Observable.create(() => {
                return this.employeesList;
            });
            return observable;
        }
        let apiUrl = this.apiUrlsService.getGridSourceUrl();
        let options: RequestOptionsArgs;

        options = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };

        var paramsAndOdata = {
            params: {
                gridName: 'Employees',
                userID: this.currentUserService.userId,
                format: "json"
            },
            odata: {
                orderby: 'FullName',
                top: 2000,
                take: 2000
            }
        };

        var payloadData = this.apiUrlsService.encodeParamsAndOdatas(paramsAndOdata);

        return this.http
            .post(this.apiUrlsService.getGridSourceUrl(), payloadData, options)
            .map((response) => {

                this.employeesList = response.json().d.results.map(function name(item, index) {
                    item.filePublicLink = item.filePublicLink || 'img/employee.png';
                    return item;
                })
                return this.employeesList
            })
            .catch(this.handleErrors);;

    }

    covertFormItemToListItem(employeeFormModel) {

    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleErrors(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
