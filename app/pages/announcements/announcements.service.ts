import {Http, Response, RequestOptionsArgs, Headers, URLSearchParams} from "@angular/Http";
import {Injectable} from "@angular/core";
import {ApiUrlsService, CurrentUserService} from '../../shared';
import {Observable} from "RxJS";
import {Announcement} from "./";
/**
 * AnnouncementsService
 */
@Injectable()
export class AnnouncementsService {
    constructor(private http: Http, private apiUrlsService: ApiUrlsService, private currentUserService: CurrentUserService) {

    }

    getAnnouncementsList() {
        let today = new Date();
        today.setHours(0, 0, 0, 0);
        let apiUrl = this.apiUrlsService.getGridSourceUrl();
        let paramsAndOdata = {
            params: {
                gridName: "Announcements",
                top: 250,
                userID: this.currentUserService.userId,
            },
            odata: {
                format: "json",
                orderby: "StartDate+asc"
            }
        };//
     
        let preparedParams = this.apiUrlsService.encodeParamsAndOdatas(paramsAndOdata);

        let options: RequestOptionsArgs = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };

        return this.http
            .post(apiUrl, preparedParams,options)
            .map((response): Announcement[] => {
                //extract data;
                return <Announcement[]> response.json().d.results;
            }, this.handleErrors)
    }

    handleErrors(error) {
        return error.json();
    }
}