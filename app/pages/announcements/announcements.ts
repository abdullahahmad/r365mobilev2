import {Page, NavController} from 'ionic-angular';
import {OnInit, Injectable} from '@angular/core';
// import {NgFor} from '@angular/common';

import {CurrentUserService, ApiUrlsService, ENV} from "../../shared";
import {Announcement, AnnouncementsService} from "./";

/*
  Generated class for the AnnouncementsPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/announcements/announcements.html',
  providers: [AnnouncementsService]
})
@Injectable()
export class AnnouncementsPage implements OnInit {
  announcements: Announcement[] = [];

  constructor(public nav: NavController, private announcementsService: AnnouncementsService) {

  }

  ngOnInit() {
    this.getAnnouncements();
  }

  getAnnouncements() {
    this.announcementsService
      .getAnnouncementsList()
      .subscribe((announcementsDataSource) => {
        this.announcements = announcementsDataSource;
      });

  }

}
