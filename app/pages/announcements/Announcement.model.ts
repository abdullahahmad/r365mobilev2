/**
 * Announcement
 */
export class Announcement {
    Id :number;
    ExpirationDate: Date;
    expirationDateString : string = "";

    StartDate : Date;
    startDateString: string ;
    
    Announcement :string;
    ThumbnailLink:string;
    
    constructor() {

    }
}