import {AnnouncementsService} from './';
import {MockBackend} from '@angular/Http/testing';
import {MockXHR} from '@angular/testing';
import {
  beforeEach,
  beforeEachProviders,
  describe,
  expect,
  it,
  inject,
  injectAsync
} from '@angular/testing';

// import {
//     TEST_BROWSER_PLATFORM_PROVIDERS,
//     TEST_BROWSER_APPLICATION_PROVIDERS
// } from '@angular/platform/testing/browser';
// setBaseTestProviders(TEST_BROWSER_PLATFORM_PROVIDERS,
//     TEST_BROWSER_APPLICATION_PROVIDERS);

describe("AnnouncementsService", () => {
    let mockHttp = new MockBackend();

    let announcementsService;
    beforeEachProviders(inject(["AnnouncementsService"],(_AnnouncementsService)=>{
        announcementsService = _AnnouncementsService;
    }));

    it('should be defined ',()=>{
        expect(announcementsService).toBeDefined();
        
    })
});