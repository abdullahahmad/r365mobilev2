import { Injectable } from '@angular/core';
import {Http, Response,RequestOptionsArgs,Headers,URLSearchParams} from "@angular/http";
import { Observable } from 'rxjs/Observable';

import {ApiUrlsService,CurrentUserService} from '../../../shared/';

@Injectable()
export class ShiftsService {

    constructor(private apiUrlsService: ApiUrlsService, private http: Http,private currentUserService:CurrentUserService) {

    }

    getShiftList(locationId ) {

        let apiUrl = this.apiUrlsService.getServiceStackUrl() + "/EmployeeSchedule";
        let payLoad = {
            
        };
        
       let searchParams = new URLSearchParams();
       searchParams.set('Location',locationId);
       searchParams.set('Date',new Date().toLocaleDateString());
       searchParams.set('Time',new Date().toLocaleTimeString());
       searchParams.set('Employee',this.currentUserService.userId);
       searchParams.set('isEmployee',"false");
        let options: RequestOptionsArgs = {
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            search: searchParams,
           
        };
    
        return this.http.get(apiUrl,options)
            .map((response) =>{ return response.json();})
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }
    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
