import {Page, NavController} from 'ionic-angular';
import {ENV,ApiUrlsService,CurrentUserService} from '../../shared';
import {Injectable} from '@angular/core';
import {ShiftsService} from './shared';

/*
  Generated class for the ShiftListPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Injectable()
@Page({
  templateUrl: 'build/pages/shift-list/shift-list.html',
  providers: [ENV,ApiUrlsService, CurrentUserService,ShiftsService]
})
export class ShiftListPage {
  shiftList: any[] = [];
  selectedLocation =  "325E1AE4-AE9A-E411-9E9B-005056870203";
  constructor(public nav: NavController, private shiftsService: ShiftsService) {

  }
  ngOnInit() {
    this.getShiftList();
  }

  getShiftList() {
    this.shiftsService
      .getShiftList(this.selectedLocation)
      .subscribe(shiftList => {
        this.shiftList = shiftList.AssignedShifts;
      });
//
  }
}