import {Page, NavController, Loading} from 'ionic-angular';
import {Injectable, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';

import {ENV, ApiUrlsService, CurrentUserService, DataSourcesService} from '../../../shared';
import {StringDatePipe} from  "../../../shared/pipes";
import {ShiftListService, ShiftListPage, IShiftsResponse} from '../shared';
import {ShiftFormPage} from "../shift-form/shift-form";
import {ShiftItemComponent} from "../components/shift-item/shift-item";

/*
  Generated class for the UnassignedShiftsPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Page({
  templateUrl: 'build/pages/shifts/unassigned-shifts/unassigned-shifts.html',
  pipes: [StringDatePipe],
  directives: [ShiftItemComponent]
})
export class UnassignedShiftsPage extends ShiftListPage {

  constructor(public nav: NavController, protected shiftsListService: ShiftListService,
    protected dataSourcesService: DataSourcesService, currentUserService:CurrentUserService) {
    super(nav, shiftsListService, dataSourcesService,currentUserService);

  }

  extractData(shiftList: IShiftsResponse) {
    this.shiftList = shiftList.UnassignedShifts;
    console.log(this.shiftList);
  }
}