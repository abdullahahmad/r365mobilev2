import {Page, NavController, Modal, NavParams, Loading, Alert, ViewController} from 'ionic-angular';
import {Injectable, OnInit} from "@angular/core";
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';

import {DataSourcesService, CurrentUserService, IDataSourceFilter, IR365Address} from '../../../shared';
import {ShiftModel, ShiftsService, ShiftListService} from "../shared";
import {StarRating} from '../../../components/star-rating';
import {AddressModal} from '../../../components/address-modal';

/*
  Generated class for the ShiftFormPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/shifts/shift-form/shift-form.html',
  providers: [ShiftsService],
  directives: [StarRating]
})
@Injectable()
export class ShiftFormPage implements OnInit {

  shiftItem: ShiftModel = new ShiftModel();;
  model: ShiftModel = new ShiftModel();
  filters: IDataSourceFilter = {};
  shiftLoader: Loading;
  locationsDatasource: any[] = [];
  loadingLocations = false;

  jobsDatasource: any[] = [];
  loadingJobs = false;

  ratingArr: number[] = [];
  ratingComplementArr = [];
  addressString: string = "";

  ResponsibilitiesDatasource: any[] = [];
  loadingResponsibilities = false;

  employeesDatasource: any[] = [];
  loadingEmployees = false;

  sitesDatasource: any[] = [];
  loadingSites = false;

  constructor(public nav: NavController, private navParams: NavParams, private dataSourcesService: DataSourcesService,
    private currentUserService: CurrentUserService, private shiftsService: ShiftsService, private shiftListService: ShiftListService, private viewCtrl: ViewController) {

  }

  ngOnInit() {
    let shiftItem: ShiftModel = new ShiftModel();
    shiftItem.Id = this.navParams.get("Id");

    this.shiftLoader = Loading
      .create({
        content: 'Loading Shift...',
        showBackdrop: true
      });
    this.nav.present(this.shiftLoader);

    this.shiftsService
      .getShift(shiftItem.Id)
      .subscribe((shiftModel) => {
        this.shiftItem = shiftModel;
        this.model = shiftModel;
        this.filters.location = this.model.Location;
        this.filters.employee = this.model.Employee;
        this.renderAddressString();
        this.shiftLoader.dismiss();
      }, () => {
        this.shiftLoader.dismiss();
      });

    this.getLocationsList();
    this.getJobsList();
    this.getEmployees();
    this.getSites();
    this.getJobsResponsibilities();


  }

  handleRatingUpdate(rating) {
    rating = rating == 0 ? null : rating;
    this.model.JobRating = rating;
    this.filters.Rating = rating;
    this.getEmployees();
  }


  getLocationsList() {

    this.loadingLocations = true;

    this.dataSourcesService
      .getLocations()
      .subscribe(locationsdatasource => {
        this.locationsDatasource = locationsdatasource;
        this.loadingLocations = false;
        this.handleLocationChanged();
        this.updateSelectedLocation();
      }, () => {
        this.loadingLocations = false;
      });
  }


  getJobsList() {

    this.loadingJobs = true;

    this.dataSourcesService
      .getJobsForLocation(this.model.Location)
      .subscribe(Jobsdatasource => {
        this.jobsDatasource = Jobsdatasource;
        this.loadingJobs = false;
        this.updateSelectedJobTitle();
      }, () => {
        this.loadingJobs = false;
      });
  }

  getEmployees() {

    this.loadingEmployees = true;

    this.dataSourcesService
      .getEmployees(this.filters)
      .subscribe(Employeesdatasource => {
        this.employeesDatasource = Employeesdatasource;
        this.loadingEmployees = false;
        this.updateSelectedEmployee();
      }, () => {
        this.loadingEmployees = false;
      });
  }

  getJobsResponsibilities() {

    this.loadingResponsibilities = true;

    this.dataSourcesService
      .getResponsibilities(this.filters)
      .subscribe(ResponsibilitiesDatasource => {
        this.ResponsibilitiesDatasource = ResponsibilitiesDatasource;
        this.loadingResponsibilities = false;
      }, () => {
        this.loadingResponsibilities = false;
      });
  }

  getSites() {

    this.loadingSites = true;

    this.dataSourcesService
      .getSites(this.filters)
      .subscribe(Sitesdatasource => {
        this.sitesDatasource = Sitesdatasource;
        this.loadingSites = false;
      }, () => {
        this.loadingSites = false;
      });
  }

  private updateSelectedLocation() {
    if (this.model.Location) {
      let selectedLocation = this.locationsDatasource.find((item, index) => {
        return item.locationId == this.model.Location;
      });
      if (selectedLocation) {
        this.model.LocationName = selectedLocation.label;
      }
    }
  }

  handleLocationChanged() {
    this.filters.location = this.model.Location;
    // this.updateSelectedLocation();
    this.getEmployees();
    this.getJobsList();
    this.getSites();
  }

  private updateSelectedJobTitle() {

    if (this.model.Job) {
      let selectedJob = this.jobsDatasource.find((item, index) => {
        return item.jobTitleId == this.model.Job;
      });
      if (selectedJob) {
        this.model.JobName = selectedJob.label;
      }
    }
  }

  handleJobTitleChanged() {
    this.filters.JobTitle = this.model.Job;
    // this.updateSelectedJobTitle();
    this.getJobsResponsibilities();
    this.getEmployees();
  }

  private updateSelectedEmployee() {

    if (this.model.Employee) {
      let selectedEmployee = this.employeesDatasource.find((item, index) => {
        return item.employeeId == this.model.Employee;
      });
      if (selectedEmployee) {
        this.model.EmployeeName = selectedEmployee.label;
      }
    }
  }

  handleEmployeeChanged() {
    console.log(arguments);
    this.filters.employee = this.model.Employee;
    // this.updateSelectedEmployee();
    this.getJobsList();
    this.getJobsResponsibilities();
  }

  handleResponsibilityChanged() {
    this.filters.responsibility = this.model.Responsibility;
    this.getEmployees();
  }

  handleDateChange() {
    console.log(this.model.dateStr, typeof (this.model.dateStr));
    console.log(this.model.startTimeString, typeof (this.model.startTimeString), moment(this.model.startTimeString).format('h:mm a'));
    console.log(this.model.endTimeString, typeof (this.model.endTimeString), moment(this.model.endTimeString).format('h:mm a'));

    this.filters.date = this.model.dateStr;
    this.filters.starttime = this.model.startTimeString;
    this.filters.endtime = this.model.endTimeString;


  }

  prepareModelToSubmit() {

    this.model.dateStr = moment(this.model.dateStr, "YYYY-MM-DD").format("M/d/YYYY");
    this.model.startTimeString = moment(this.model.startTimeString).format('h:mm a');
    this.model.endTimeString = moment(this.model.endTimeString).format('h:mm a');
  }

  renderAddressString() {
    this.addressString = "";
    if (this.model.Address1) {
      this.addressString += this.model.Address1 || "";
      if (this.model.Address2) {
        this.addressString += ", ";
      } else {
        this.addressString += " ";
      }
    }
    this.addressString += (this.model.Address2 || "" )+ ` \n`;
    this.addressString += `${this.model.City || ""} ${this.model.State ? ", " + this.model.State + " " : ""} ${this.model.Country || ""}\n`;
    this.addressString += this.model.Zip || "";
  }

  openAddressForm() {
    let address: IR365Address = {
      Address1: this.model.Address1,
      Address2: this.model.Address2,
      City: this.model.City,
      State: this.model.State,
      Country: this.model.Country,
      Zip: this.model.Zip
    }
    let addressModal = Modal.create(AddressModal, { address: address });
    this.nav.present(addressModal);

    addressModal.onDismiss((updatedAddress: IR365Address) => {
      Object.assign(this.model,updatedAddress);

      this.renderAddressString();
    });
  }

  submit() {

    this.prepareModelToSubmit();
    let pubSub: Observable<any>;
    this.shiftLoader = Loading
      .create({
        content: 'Saving Shift...',
        showBackdrop: true
      });
    this.nav.present(this.shiftLoader);

    if (this.model.Id) {
      pubSub = this.shiftsService.updateShift(this.model);
    } else {
      pubSub = this.shiftsService.addShift(this.model);
    }


    pubSub
      .subscribe((data) => {
        this.shiftLoader.dismiss();
        this.updateSelectedLocation();
        this.updateSelectedJobTitle();
        this.updateSelectedEmployee();
        this.shiftListService.updateShift(this.model);
        this.nav.pop();
      }, (error) => {
        this.shiftLoader.dismiss();
        let alert = Alert.create({
          title: 'Failed To Update Shift',
          subTitle: error,
          buttons: ['OK']
        });
        this.nav.present(alert);
      })

  }
}
