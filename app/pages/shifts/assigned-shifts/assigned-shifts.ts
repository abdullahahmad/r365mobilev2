import {Page, NavController, Loading} from 'ionic-angular';
import {Injectable, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import { Observable } from 'rxjs/Observable';


import {ENV, ApiUrlsService, CurrentUserService, DataSourcesService} from '../../../shared';
import {StringDatePipe} from  "../../../shared/pipes";
import {ShiftListService, ShiftsService, ShiftListPage, IShiftsResponse, shiftListItem} from '../shared';
import {ShiftFormPage} from '../shift-form/shift-form';
import {ShiftItemComponent} from '../components/shift-item/shift-item'
/*
  Generated class for the ShiftListPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Injectable()
@Page({
  templateUrl: 'build/pages/shifts/assigned-shifts/assigned-shifts.html',
  pipes: [StringDatePipe],
  // providers: [ShiftListService],
  directives: [ShiftItemComponent],
})
export class AssignedShiftsPage extends ShiftListPage {

  constructor(public nav: NavController, protected shiftsListService: ShiftListService,
    protected dataSourcesService: DataSourcesService, currentUserService: CurrentUserService) {
    super(nav, shiftsListService, dataSourcesService, currentUserService);

  }

  extractData(shiftList: IShiftsResponse) {
    // this.loading.destroy();
    this.shiftList = shiftList.AssignedShifts;
  }

  doRefresh(refresher) {
    console.log("refresh done", refresher);
    this.getShiftList(refresher);
  }
}
