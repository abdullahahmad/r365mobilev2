import {Page, NavController} from 'ionic-angular';
import {Component} from "@angular/core";
import {AssignedShiftsPage} from "./assigned-shifts/assigned-shifts";
import {UnassignedShiftsPage} from "./unassigned-shifts/unassigned-shifts";
/*
  Generated class for the ShiftsTabsPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/shifts/shifts-tabs.html',
})
export class ShiftsTabsPage {

  assignedShiftsPage = AssignedShiftsPage;
  unassignedShiftsPage = UnassignedShiftsPage;

  constructor(public nav: NavController) {
    
  }
}
