import {R365GUID, IR365Address} from "../../../shared";
/*
{
    "startTimeString":"8:00 AM","endTimeString":"12:00 PM",
    "dateStr":"6/30/2016","ScheduleName":"Week End 7/5/2016",
    "Id":"BC4EF1C0-4CBD-4A9E-8E39-21C1F15A7E8C",
    "Type":1,"Date":"\/Date(1467262800000-0000)\/","StartTime":"\/Date(1467291600000-0000)\/","EndTime":"\/Date(1467306000000-0000)\/",
    "Location":"325E1AE4-AE9A-E411-9E9B-005056870203","Job":"24C2006D-C8CA-E511-9400-0CC47A39187C","Employee":"B0192F12-D007-4B2F-93D7-760500228923",
    "Schedule":"4A10D26D-196D-4DFC-8FC8-AB2013A2453F","CreatedOn":"\/Date(1464610622860-0000)\/","CreatedBy":"798ae83c1e9e4b018356abcb2ed66184",
    "ModifiedOn":"\/Date(1466324143837-0000)\/","ModifiedBy":"b0192f12d0074b2f93d7760500228923"}
*/
export enum ShiftType {
    NONE,
    SHIFT,
    TIMEOFF,
    EVENT
}

export class ShiftModel implements IR365Address {

    Id: R365GUID;

    Address1: string;
    Address2: string;
    City: string;
    State: string;
    Zip: string;
    Country: string;

    startTimeString: string;
    endTimeString: string;
    dateStr: string;
    Job: R365GUID;
    JobName: string;
    Employee: R365GUID;
    EmployeeName: string;
    Location: R365GUID;
    LocationName: string;
    Responsibility: string;
    ResponsibilityName: string;
    JobRating: number;
    Type: ShiftType; // 1 shift. 2 time off , 3 event

    constructor() {
    }
    static formatForForm(ShiftModel) {
        let formReadyShift = new ShiftModel();
        formReadyShift.Id = ShiftModel.Id;
    }
}

export enum IRequestStatus {
    NONE,
    REQUESTED,
    APPROVED,
    DENIED,
    TRADE_PROPOSED,
    TRADE_ACCEPTED,
    TRADE_REJECTED
}

export enum IRequestType {
    NONE,
    TIME_OFF,
    SHIFT_TRADE,
    SHIFT_DROP,
    CLAIM_SHIFT,
    CLAIM

}

export class shiftListItem {
    EventsID: R365GUID;

    ActualHours: string;
    Date: string;
    EmployeeId: R365GUID;
    EmployeeName: string;
    EndTime: string;
    JobId: R365GUID;
    JobName: string;
    Request: IRequestType;
    RequestStatus: IRequestStatus;
    ScheduleHours: string;
    StartTime: string;
    Type: string;
}

export interface IShiftsResponse {
    AssignedShifts: shiftListItem[],
    UnassignedShifts: shiftListItem[]
}