import { Injectable } from '@angular/core';
import {Http, Response, RequestOptionsArgs, Headers, URLSearchParams} from "@angular/http";
import { Observable } from 'rxjs/Observable';

import {ApiUrlsService, CurrentUserService, R365GUID} from '../../../shared/';
import {ShiftModel, ShiftListService} from "./";
import * as moment from 'moment';

@Injectable()
export class ShiftsService {

    constructor(private apiUrlsService: ApiUrlsService, private http: Http,
        private currentUserService: CurrentUserService) {

    }

    getShift(shiftId: R365GUID): Observable<ShiftModel> {

        let apiUrl = this.apiUrlsService.getServiceStackUrl() + "/ScheduleEvent/" + shiftId;
        let options: RequestOptionsArgs;

        options = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };

        return this.http
            .get(apiUrl, options)
            .map((response) => {
                let model: ShiftModel = response.json().model;

                model.dateStr = moment(model.dateStr, "MM/DD/YYYY").format("YYYY-MM-DD");
                model.startTimeString = moment(model.startTimeString, "h:mm A").format();
                model.endTimeString = moment(model.endTimeString, "h:mm A").format();
                return <ShiftModel>model;
            })
            .catch(this.handleErrors);
    }

    updateShift(shiftModel: ShiftModel) {

        let shiftId = shiftModel.Id;
        let scheduleEvent: any = shiftModel;
        let apiUrl = this.apiUrlsService.getServiceStackUrl() + "/ScheduleEvent";
        let payload: any;
        let options: RequestOptionsArgs;

        // scheduleEvent.Date = undefined;
        scheduleEvent.CreatedOn = undefined;
        scheduleEvent.ModifiedOn = undefined;
        scheduleEvent.EndTime = undefined;
        scheduleEvent.StartTime = undefined;

        payload = {
            userId: this.currentUserService.userId,
            scheduleEvent: scheduleEvent,
            Id: shiftId
        };
        options = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };
        return this.http
            .put(apiUrl, JSON.stringify(payload), options)
            .map((response) => {
                let data = response.json();
                // this.shiftListService.updateShift(shiftModel);

                if (data.validation.validationMessage) {
                    throw {
                        message: "Failed To Update Shift Due to Validation Error: " + data.validation.validationMessage,
                        status: 422,
                        statusText: "Validation Error"
                    };
                }
                return data.model;
            }, this.handleErrors)
            .catch(this.handleErrors);
    }

    addShift(shiftModel) {
        let options: RequestOptionsArgs;

        options = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };

        return this.http
            .post(this.apiUrlsService.getServiceStackUrl() + "/ScheduleEvent", JSON.stringify(shiftModel), options)
            .map((response) => {
                let data = response.json();

                return data.model;
            }, this.handleErrors)
            .catch(this.handleErrors);
    }

    deleteShift(shiftId: R365GUID) {
        throw "Not Implemented yet ";
    }

    prepeareShiftForForm(rawShift) {


    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleErrors(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.log(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
