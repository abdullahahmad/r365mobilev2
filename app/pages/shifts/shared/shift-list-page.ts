import {Page, NavController, Loading} from 'ionic-angular';
import {Injectable, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import { Observable } from 'rxjs/Observable';

import {DataSourcesService, CurrentUserService} from '../../../shared';
import {ShiftListService, IShiftsResponse, shiftListItem} from '../shared';
import {ShiftFormPage} from '../shift-form/shift-form'

export abstract class ShiftListPage implements OnInit {

    shiftList: shiftListItem[] = [];
    selectedLocation;
    locationsDatasource = [];
    loadingLocations: boolean;
    loading: Loading;

    constructor(public nav: NavController, protected shiftsListService: ShiftListService,
        protected dataSourcesService: DataSourcesService, private currentUserService: CurrentUserService) {
    }

    ngOnInit() {
        this.getLocationsList();
        this.selectedLocation = this.currentUserService.getDefaultLocation();
        if (this.selectedLocation) {
            this.getShiftList();
        }
    }

    getShiftList(refresher?: any) {
        this.loading = Loading
            .create({
                content: 'Loading Shifts...',
                showBackdrop: true
            });
        this.nav.present(this.loading);

        this.shiftsListService
            .getShiftList(this.selectedLocation)
            .subscribe((data) => {
                this.loading.dismiss();
                this.extractData(data);
                if (refresher) {
                    refresher.complete();
                }
            }, () => {
                this.loading.dismiss();
            });
    }

    abstract extractData(shiftList: IShiftsResponse);

    goToItemDetails(item) {
        this.nav.push(ShiftFormPage, { Id: item.EventsID })
    }

    getLocationsList() {

        this.loadingLocations = true;

        this.dataSourcesService
            .getLocations()
            .subscribe(locationsdatasource => {
                this.locationsDatasource = locationsdatasource;
                this.loadingLocations = false;
            }, () => {
                this.loadingLocations = false;
            });
    }

    changeLocation() {
        this.getShiftList();
    }

}
