import { Injectable } from '@angular/core';
import {Http, Response, RequestOptionsArgs, Headers, URLSearchParams} from "@angular/http";
import { Observable } from 'rxjs/Observable';

import {ApiUrlsService, CurrentUserService} from '../../../shared/';
import {shiftListItem, ShiftModel, IShiftsResponse} from "./";

@Injectable()
export class ShiftListService {

    shifts: IShiftsResponse;

    constructor(protected apiUrlsService: ApiUrlsService, protected http: Http, protected currentUserService: CurrentUserService) {

    }

    getShiftList(locationId): Observable<IShiftsResponse> {

        let apiUrl = this.apiUrlsService.getServiceStackUrl() + "/EmployeeSchedule";
        let options: RequestOptionsArgs;
        let searchParams = new URLSearchParams();

        locationId = locationId || this.currentUserService.getDefaultLocation();
        searchParams.set('Location', locationId);
        searchParams.set('Date', new Date().toLocaleDateString());
        searchParams.set('Time', new Date().toLocaleTimeString());
        searchParams.set('Employee', this.currentUserService.userId);
        searchParams.set('isEmployee', "false");

        options = {
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            search: searchParams
        };

        return this.http
            .get(apiUrl, options)
            .map((response): IShiftsResponse => {
                this.shifts = response.json();
                return this.shifts;
            })
            .catch(this.handleError);
    }


    updateShift(shiftFormItem: ShiftModel) {
        let myshift: shiftListItem;
        let shiftId = shiftFormItem.Id.toLowerCase().replace(/-/g, "");
        if (shiftFormItem.Employee) {
            myshift = this.shifts.AssignedShifts.find(function (value, index) {
                return shiftId === value.EventsID.toLowerCase().replace(/-/g, "");
            });

            let itemIndex = this.shifts.AssignedShifts.indexOf(myshift);


        } else {
            myshift = this.shifts.UnassignedShifts.find(function (value, index) {
                return shiftId === value.EventsID.toLowerCase().replace(/-/g, "");
            });
        }

        myshift.Date = shiftFormItem.dateStr;
        myshift.StartTime = shiftFormItem.startTimeString;
        myshift.EndTime = shiftFormItem.endTimeString;

        myshift.EmployeeName = shiftFormItem.EmployeeName;
        myshift.EmployeeId = shiftFormItem.Employee;

        myshift.JobId = shiftFormItem.Id;
        myshift.JobName = shiftFormItem.JobName;

        console.log(shiftFormItem, myshift);
    }

    private handleError(error: any): Observable<IShiftsResponse> {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
