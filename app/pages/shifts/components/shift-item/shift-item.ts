import {Component,Input} from '@angular/core';
import {StringDatePipe} from '../../../../shared/pipes'
/*
  Generated class for the ShiftItem component.

  See https://angular.io/docs/ts/latest/api/core/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'shift-item',
  templateUrl: 'build/pages/shifts/components/shift-item/shift-item.html',
  pipes:[StringDatePipe]
})
export class ShiftItemComponent {
  @Input() shift;
  @Input() showLocation;
  constructor() {
  }
}
