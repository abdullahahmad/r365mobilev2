import {ViewChild, provide, Component} from '@angular/core';
import {ionicBootstrap, Events, Platform, MenuController, Nav, ViewController} from 'ionic-angular';
import {Http} from '@angular/Http';
import {StatusBar} from 'ionic-native';

import {CurrentUserService, ApiUrlsService, AuthService, ENV, DataSourcesService} from "./shared";
import {ShiftListService} from "./pages/shifts/shared";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {LoginPage} from './pages/login';
import {AnnouncementsPage} from "./pages/announcements";
import {EmployeesListPage} from "./pages/employees-list";
import {ShiftsTabsPage} from './pages/shifts/shifts-tabs';
import {ChangePasswordPage} from './pages/change-password/change-password';


interface PageObj {
  title: string;
  component: any;
  icon?: string;
  index?: number;
}


@Component({
  templateUrl: 'build/app.html',
  providers: [ENV, ApiUrlsService, AuthService, CurrentUserService, DataSourcesService, ShiftListService]
})
class r365App {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage: any = ShiftsTabsPage;
  appPages: Array<PageObj>;
  loggedOutPages: Array<PageObj>;
  loggedInPages: Array<PageObj>;

  constructor(
    private events: Events,
    private platform: Platform,
    private menu: MenuController,
    private authService: AuthService
  ) {
    this.initializeApp();

    // set our app's pages
    this.appPages = [
      { title: 'Employees', component: EmployeesListPage },
      { title: 'Announcements', component: AnnouncementsPage },
      { title: 'Shifts', component: ShiftsTabsPage },
      { title: 'Employees', component: EmployeesListPage }
    ];
    this.loggedOutPages = [
      { title: 'Login', component: LoginPage }
    ];
    this.loggedInPages = [
      { title: 'My Profile', component: LoginPage },
      { title: 'Change Passowrd', component: ChangePasswordPage },
      { title: 'Logout', component: LoginPage }
    ]

    this.listenToLoginEvents();

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.//
      if (!this.authService.isLoggedIn()) {
        this.nav.setRoot(LoginPage);
      } else if (this.authService.shouldIChangeThePassword()) {
        this.nav.setRoot(ChangePasswordPage);
      } else {
        this.enableMenu(true);
      }
      StatusBar.styleDefault();
      this.nav.viewWillEnter.subscribe((view: ViewController) => {
        if (!this.authService.isLoggedIn() && view.componentType != LoginPage) {

          this.nav.setRoot(LoginPage);
          throw "Not Logged In";
        }
        // view.dismiss();
        console.log("nav will enter", view);
      });
    });
  }
  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);

    if (page.title === "Logout") {
      setTimeout(() => {
        this.authService.logout();
      }, 200)
    }
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.enableMenu(true);
      if (this.authService.shouldIChangeThePassword()) {
        this.nav.setRoot(this.rootPage);
      }
    });

    this.events.subscribe('user:login:youShoudlChangeYourPassword', () => {
      this.nav.setRoot(ChangePasswordPage);
    });

    this.events.subscribe('user:logout', () => {
      this.enableMenu(false);
    });
  }

  enableMenu(loggedIn) {
    this.menu.enable(loggedIn, 'loggedInMenu');
  }
}

ionicBootstrap(r365App);