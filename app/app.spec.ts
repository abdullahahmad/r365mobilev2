import { ADDITIONAL_TEST_BROWSER_PROVIDERS, TEST_BROWSER_STATIC_PLATFORM_PROVIDERS } from '@angular/platform-browser/testing/browser_static';
import { BROWSER_APP_DYNAMIC_PROVIDERS }                from '@angular/platform-browser-dynamic';
import { resetBaseTestProviders, setBaseTestProviders } from '@angular/core/testing';
import { R365App }                                   from './app';

resetBaseTestProviders();
setBaseTestProviders(
  TEST_BROWSER_STATIC_PLATFORM_PROVIDERS,
  [
    BROWSER_APP_DYNAMIC_PROVIDERS,
    ADDITIONAL_TEST_BROWSER_PROVIDERS,
  ]
);

let r365App: R365App = null;

class MockClass {
  public ready(): any {
    return new Promise((resolve: Function) => {
      resolve();
    });
  }

  public close(): any {
    return true;
  }

  public setRoot(): any {
    return true;
  }
}

describe('ClickerApp', () => {

  beforeEach(() => {
    let mockClass: any = (<any>new MockClass());
    r365App = new R365App(mockClass, mockClass);
  });

  it('initialises with two possible pages', () => {
    expect(r365App['pages'].length).toEqual(4);
  });

  it('initialises with a root page', () => {
    expect(r365App['rootPage']).not.toBe(null);
  });

  it('initialises with an app', () => {
    expect(r365App['app']).not.toBe(null);
  });

  it('opens a page', () => {
    spyOn(r365App['menu'], 'close');
    // cant be bothered to set up DOM testing for app.ts to get access to @ViewChild (Nav)
    r365App['nav'] = (<any>r365App['menu']);
    spyOn(r365App['nav'], 'setRoot');
    r365App.openPage(r365App['pages'][1]);
    expect(r365App['menu']['close']).toHaveBeenCalled();
  });
});
