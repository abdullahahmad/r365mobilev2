import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';

import {ENV} from './dev.configs';

@Injectable()
export class ApiUrlsService {
    storage = window.localStorage;
    fixedGlobalSubDomain: string = "dev";
    fixedGlobalVersion: string = "dev";

    databaseName: string;
    versionName: string;

    links = {

        login: "/ServiceStack/auth/credentials",
        loggedInUserInfo: "/ServiceStack/SessionData/",
        logout: "/ServiceStack/auth/logout",
        stockCountList: "/SQLProc/Mobile.asmx/getStockCountList",
        stockCountTemplatesList: "/SQLProc/Mobile.asmx/getStockCountTemplateList",
        stockCountDetails: "/SQLProc/Mobile.asmx/getStockCountDetails",
        saveStockCount: "/SQLProc/Mobile.asmx/SaveStockCount",
        transactionRibbon: "/ServiceStack/TransactionRibbon",
        jasperReportsPreferences: "/ServiceStack/JasperReports",
        useTransaction: "/SQLProc/Actions.asmx/useTransaction",
        dailySalesReport: "/SQLProc/Retrieve.asmx/getDailySalesReport",
        getReportURL: "/SQLProc/Actions.asmx/getReportURL",
        getPreferencesVersion: "/SQLProc/Mobile.asmx/getPreferencesVersion",
        getGridSourceUrl: "/SQLProc/Grids.asmx/getGridSource",
        getLocationsUrl: "/SQLProc/Retrieve.asmx/getLocations",
    };

    constructor(private http: Http, private ENV: ENV) {

        if (this.ENV.is("dev")) {
            Object.keys(this.links).forEach((key) => {

                if (!!~this.links[key].indexOf("/SQLProc")) {
                    this.links[key] = this.links[key].replace("/SQLProc", this.ENV.dev.SQLProcPort);
                }
                if (!!~this.links[key].indexOf("/ServiceStack")) {
                    this.links[key] = this.links[key].replace("/ServiceStack", this.ENV.dev.ServiceStackPort);
                }
            });
        }

    }


    public getUserSubDomainsByEmail(email): Observable<any[]> {
        let apiUrl = "https://" + this.fixedGlobalSubDomain + ".restaurant365.net/" + this.fixedGlobalVersion + "/SQLProc/Retrieve.asmx/getEmailSubDomain";
        //return "https://" + getDbName() + ".restaurant365.net/" + getVersionName();
        let payLoad = JSON.stringify({ email: email });
        let options: RequestOptionsArgs = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };

        return this.http
            .post(apiUrl, payLoad, options)
            .map((res: Response) => {
                let body = res.json() || {};
                let subdomains = body.d;

                if (subdomains && subdomains.length > 0) {
                    return subdomains;
                }
                else {
                    return [];
                }
            }, (errorResponse) => {
                console.error("get use domains ", errorResponse.data);
                return [];
            })
            .catch((error) => {
                // In a real world app, we might use a remote logging infrastructure
                // We'd also dig deeper into the error to get a better message
                let errMsg = (error.message) ? error.message :
                    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
                console.error(errMsg); // log to console instead
                return Observable.throw(errMsg);
            });

    }


    encodeParamsAndOdatas(parmasAndOdataObject) {
        var paramsStrArr = [];
        for (var key in parmasAndOdataObject.params) {
            paramsStrArr.push("" + key + "=" + parmasAndOdataObject.params[key]);
        }
        var odataStrArr = [];
        for (var key in parmasAndOdataObject.odata) {
            odataStrArr.push("" + key + "=" + parmasAndOdataObject.odata[key]);
        }
        var filters = [];
        var passedFilters = parmasAndOdataObject.filter;


        var stringBeforeRequest2 = {
            text: paramsStrArr.join("&") + "&%24" + odataStrArr.join("&%24")
        };
        if (passedFilters) {
            var filterString = passedFilters.filters.map(function (item, index) {
                switch (item.operation) {
                    case 'eq':
                    case 'neq':
                    case 'gt':
                    case 'gte':
                    case 'lt':
                    case 'lte': {
                        if (!!Date.parse(item.value) || !isNaN(Date.parse(item.value))) {
                            var value = item.value.toISOString().replace(/:/g, "%3A");
                            value = value.substring(0, value.indexOf("."));
                            return "(" + item.field + "+" + item.operation + "+datetime'" + value + "')";
                        }
                        return "(" + item.field + "+" + item.operation + "+'" + item.value + "')";
                    }
                    case 'startswith':
                    case 'endswith': {
                        return item.operation + "(" + item.field + "%2C'" + item.value + "')";

                    }
                    case 'contains':
                    case 'substringof': {
                        return item.operation + "('" + item.value + "'%2C" + item.field + ")";

                    }
                    case 'substringof(tolower':
                        return item.operation + "('" + item.value + "'%2C" + item.field + "))";

                    default:
                        return "" + item.field + "+" + item.operation + "+'" + item.value + "'";

                }
            }).join(passedFilters.logic ? " " + passedFilters.logic + " " : "+%and%+");

            stringBeforeRequest2.text += "&%24filter=" + filterString + "";
        }
        // thats not my fault !!! its a back end fault ! !!!
        var jsonString = JSON.stringify(stringBeforeRequest2);
        var lastIndex = jsonString.lastIndexOf('"');
        var withclosing = jsonString.substring(0, lastIndex) + '"}';
        return jsonString;
    }

    getGridSourceUrl() {
        return this.getWebServiceUrl() + "/Grids.asmx/getGridSource";
    }

    getRetriveSourceUrl() {
        return this.getWebServiceUrl() + "/Retrieve.asmx";
    }

    getServiceStackUrl() {
        if (this.ENV.is('dev')) {
            return this.ENV.dev.ApiEndPoint + this.ENV.dev.ServiceStackPort;
        }
        return this.getAppFullPath() + "/ServiceStack";
    }

    getWebServiceUrl() {
        if (this.ENV.is('dev')) {
            return this.ENV.dev.ApiEndPoint + this.ENV.dev.SQLProcPort;
        }
        return this.getAppFullPath() + "/SQLProc";
    }
    
    getBackendUrl(service) {

        var url = this.links[service] ? this.links[service] : "";
        // if (this.ENV.is('dev')) {
        //     return this.ENV.dev.ApiEndPoint + this.ENV.dev.SQLProcPort + url;
        // }
        return this.getAppFullPath() + url;
    }

    getAppFullPath() {
        if (this.ENV.is('dev')) {
            return this.ENV.dev.ApiEndPoint;
        } else {
            return "https://" + this.getDbName() + ".restaurant365.net/" + this.getVersionName();

        }
    }

    getDbName() {
        if (!this.databaseName) {
            this.databaseName = this.storage.getItem("DbName");
        }
        return this.databaseName;
    }

    getVersionName() {
        if (this.ENV.is('dev') || this.versionName == null && this.storage.getItem("VersionName") == null) {
            this.versionName = "Dev";
        } else if (this.storage.getItem("VersionName") != null) {
            this.versionName = this.storage.getItem("VersionName");
        } else {
            this.versionName = "Dev";
        }
        return this.versionName;
    }

    setVersionName(newVersionName) {
        this.versionName = newVersionName;
        this.storage.setItem("VersionName", this.versionName);
    }

    setDbName(newName) {
        if (this.ENV.is('dev')) {
            this.databaseName = "dev";
            this.storage.setItem("DbName", newName);
        } else {
            this.databaseName = newName;
            this.storage.setItem("DbName", newName);
        }
    }
}
