import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {ApiUrlsService} from './ApiUrls.service';
import {ENV} from './dev.configs';

describe("API_Url_service", () => {
    let env = new ENV();
    env.selectedEnv = 'pro';

    let apiUrlService = new ApiUrlsService(<any>{}, env);

    it('should be defined ', () => {
        expect(apiUrlService).toBeDefined();
        expect(apiUrlService.getAppFullPath).toBeDefined();
        expect(apiUrlService.getGridSourceUrl).toBeDefined();
        expect(apiUrlService.getRetriveSourceUrl).toBeDefined();
        expect(apiUrlService.getServiceStackUrl).toBeDefined();
        expect(apiUrlService.getBackendUrl).toBeDefined();
        expect(apiUrlService.getAppFullPath).toBeDefined();
        expect(apiUrlService.getDbName).toBeDefined();
        expect(apiUrlService.getVersionName).toBeDefined();
        expect(apiUrlService.setVersionName).toBeDefined();
        expect(apiUrlService.setDbName).toBeDefined();
    });

    describe('test Links ', () => {

        it('should have the following links', () => {
            var links = {

                login: "/ServiceStack/auth/credentials",
                loggedInUserInfo: "/ServiceStack/SessionData/",
                logout: "/ServiceStack/auth/logout",
                stockCountList: "/SQLProc/Mobile.asmx/getStockCountList",
                stockCountTemplatesList: "/SQLProc/Mobile.asmx/getStockCountTemplateList",
                stockCountDetails: "/SQLProc/Mobile.asmx/getStockCountDetails",
                saveStockCount: "/SQLProc/Mobile.asmx/SaveStockCount",
                transactionRibbon: "/ServiceStack/TransactionRibbon",
                jasperReportsPreferences: "/ServiceStack/JasperReports",
                useTransaction: "/SQLProc/Actions.asmx/useTransaction",
                dailySalesReport: "/SQLProc/Retrieve.asmx/getDailySalesReport",
                getReportURL: "/SQLProc/Actions.asmx/getReportURL",
                getPreferencesVersion: "/SQLProc/Mobile.asmx/getPreferencesVersion",
                getGridSourceUrl: "/SQLProc/Grids.asmx/getGridSource",
                getLocationsUrl: "/SQLProc/Retrieve.asmx/getLocations",
            };
            expect(apiUrlService.links).toEqual(links);
        })
    })


});
