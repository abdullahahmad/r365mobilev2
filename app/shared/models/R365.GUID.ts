export class R365GUID extends String {

    constructor() {
        super();
        this.toLowerCase();
    }

    toString() {
        return this.toLowerCase();
    }

    removeDahses() {
        let s = this.replace(/-/g, "");
        return s;
    }

    addDashes() {
        return R365GUID.addDahses(this);
    }

    static addDahses(unDashedId: string | String) {
        //"325E1AE4-AE9A-E411-9E9B-005056870203";
        // 8 13 18 23
        if (unDashedId.indexOf('-') >= -1)
            return unDashedId;
        let dashedGuid = [
            unDashedId.substring(0, 8),
            unDashedId.substring(8, 12),
            unDashedId.substring(12, 16),
            unDashedId.substring(16, 20),
            unDashedId.substring(20),
        ].join("-");
    }
    static getGuid(param: R365GUID | string) {
        let guid: R365GUID = new R365GUID();
        guid.concat(param.toString());
        return guid;
    }
    static isGuid(param: R365GUID | string) {
        let isGuid = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(param.toString()) || /^[0-9a-f]{8}[0-9a-f]{4}[1-5][0-9a-f]{3}[89ab][0-9a-f]{3}[0-9a-f]{12}$/i.test(param.toString());
        if (!isGuid)
            console.warn("Invalid Guid");
        return isGuid;
    }

    normalize() {
        return this.removeDahses().toLowerCase();
    }

    isEquals(peer: R365GUID | string) {
        let compare = new R365GUID();
        compare = R365GUID.getGuid(peer);
        if (!R365GUID.isGuid(peer)) {
            return false;
        }
        return this.normalize() == compare.normalize();
    }
}