export interface IR365Address {
    Address1: string;
    Address2: string;
    City: string;
    State: string;
    Zip: string;
    Country: string;
}