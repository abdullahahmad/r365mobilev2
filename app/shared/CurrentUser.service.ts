import {Http, Response, Headers, RequestOptionsArgs} from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

import {ApiUrlsService} from './';

@Injectable()
export class CurrentUserService {
    private USER_DATA_STORAGE_KEY = "UserData";

    userId;
    userData;
    userRole;

    constructor(private http: Http, private apiUrlsService: ApiUrlsService) {

        this.getCurrentUserData();
    }

    loadLoggedInUserInfo(sessionId) {
        let apiUrl = this.apiUrlsService.getBackendUrl("loggedInUserInfo") + sessionId;

        let options: RequestOptionsArgs = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };
        return this.http.get(apiUrl, options)
            .toPromise()
            .then(response => {

                let data = response.json();
                this.setCurrentUserData(data);
                return this.initUserPreferences().then(() => {
                    return this.userData;
                });
            });
    }

    initUserPreferences() {

        let apiUrl = this.apiUrlsService.getBackendUrl("getPreferencesVersion");
        let payload = {
            initDb: this.apiUrlsService.getDbName(),
            userId: this.userData.userId
        };
        let options: RequestOptionsArgs = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };

        return this.http.post(apiUrl, JSON.stringify(payload), options)
            .toPromise()
            .then(response => {
                var preferencesVersion = response.json().d;
                this.apiUrlsService.setVersionName(preferencesVersion);
                return true;
            });

    }

    getCurrentUserData() {
        if (!this.userData) {
            this.userData = JSON.parse(localStorage.getItem(this.USER_DATA_STORAGE_KEY));
        }
        this.userId = this.userData ? this.userData.userId : null;
        return this.userData;
    }

    setCurrentUserData(userData) {
        this.userData = userData;
        this.userId = this.userData.userId;
        localStorage.setItem(this.USER_DATA_STORAGE_KEY, JSON.stringify(this.userData));
    }

    clearUserData() {
        this.userData = null;
        localStorage.removeItem(this.USER_DATA_STORAGE_KEY);
    }

    getDefaultLocation() {
        let userData = this.getCurrentUserData();
        let userLocation = userData ? userData.defaultLocation || "" : "";
        return userLocation.toUpperCase();
    }

    hasRole(roleName) {

    }

    loginTypeIs(intendedLoginType) {
        return this.userData.type === intendedLoginType;
    }
}
