import {Http, Response, RequestOptionsArgs, URLSearchParams, Headers} from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import {Alert, Events} from 'ionic-angular';

import {ApiUrlsService} from './';
import {LoginModel} from '../pages/login/shared';
import {CurrentUserService} from './';

@Injectable()
export class AuthService {
    private USER_SESSIONM_KEY = "UserSessionData";

    constructor(private events: Events, private currentUserService: CurrentUserService, private apiUrlsService: ApiUrlsService, private http: Http) { }

    isLoggedIn() {
        return !!this.currentUserService.getCurrentUserData();
    }

    login(model: LoginModel) {
        let apiUrl = this.apiUrlsService.getBackendUrl("login");
        var payload = model;
        let options: RequestOptionsArgs = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };


        return this.http.post(apiUrl, JSON.stringify(model), options)
            .toPromise()
            .then(response => {

                var UserSessionData = response.json();
                if (response.status == 200 && UserSessionData && !UserSessionData.ErrorCode) {

                    this.setLoggedInUserSession(UserSessionData);
                  return  this.currentUserService
                        .loadLoggedInUserInfo(UserSessionData.SessionId)
                        .then((currentUserData) => {

                            if (currentUserData.dm_changePassword == 1) {
                                this.events.publish('user:login:youShoudlChangeYourPassword');
                            } else {
                                this.events.publish('user:login');
                            }
                            return currentUserData;
                        });
                } else {
                    return Promise.reject(false);
                }
            }, err => { return err.json.data(); });
    }

    logout() {

        let apiUrl = this.apiUrlsService.getBackendUrl("logout");

        return this.http.post(apiUrl, "")
            .subscribe(data => {
                this.clearLoggedInUserSession();
                this.currentUserService.clearUserData();

                this.events.publish('user:logout');
            }, err => {
                this.clearLoggedInUserSession();
                this.currentUserService.clearUserData();

                this.events.publish('user:logout');
                return err;
            });
    }

    shouldIChangeThePassword() {

        if (!this.isAuthenticated()) return false;

        let userData = this.currentUserService.getCurrentUserData();
        if (!userData) return false;
        if (userData.dm_changePassword == 1) return true;
        return false;
    }


    changeUserPassword(oldPassword, newPassword, confirmPassword?) {
        let userData = this.currentUserService.getCurrentUserData();
        let apiUrl = this.apiUrlsService.getServiceStackUrl() + "/UpdatePassword?";
        let data = {
            oldPassword: oldPassword,
            newPassword: newPassword,
            userId: this.currentUserService.userId
        };
        let searchParams = new URLSearchParams();
        let options: RequestOptionsArgs = {
            headers: new Headers({
                'Content-Type': "text/plain",
                'Accept': "application/json, text/plain, */*",
            }),
            search: searchParams
        };

        return this.http.put(apiUrl, JSON.stringify(data), options)
            .map(response => {
                var data = response.json();
                return data;
            }, (error) => {
                return error.json();
            });

    }


    sendResetPasswordRequest(userEmail: string) {

        let apiUrl = this.apiUrlsService.getWebServiceUrl() + "/Actions.asmx/ResetPassword";
        let payload = {
            userName: userEmail
        };
        let options: RequestOptionsArgs;

        options = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };

        return this.http.post(apiUrl, JSON.stringify(payload), options).toPromise();
    }

    getLoggedInUserSession() {
        return JSON.parse(localStorage.getItem(this.USER_SESSIONM_KEY));
    }

    setLoggedInUserSession(userSessionData) {
        localStorage.setItem(this.USER_SESSIONM_KEY, JSON.stringify(userSessionData));
    }


    clearLoggedInUserSession() {
        localStorage.removeItem(this.USER_SESSIONM_KEY);
    }

    isAuthenticated() {

        return !!this.getLoggedInUserSession();
    }


}