import {Http, Response, Request, Headers, RequestOptionsArgs, URLSearchParams} from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

import {ApiUrlsService, CurrentUserService, R365GUID} from './';

@Injectable()
export class DataSourcesService {

    constructor(private apiUrlsService: ApiUrlsService, private http: Http, private currentUserService: CurrentUserService) { }

    getLocations() {
        let apiUrl = this.apiUrlsService.getBackendUrl("getLocationsUrl");

        let requestParams: Object;

        requestParams = {
            transactionType: 0,
            userId: this.currentUserService.userId
        };

        return this.getDataSourceSource(apiUrl, requestParams);
    }

    getJobsForLocation(locationId?: R365GUID | string) {

        let apiUrl = this.apiUrlsService.getRetriveSourceUrl() + "/getJobTitles";
        let requestParams: Object;

        requestParams = {
            location: locationId || this.currentUserService.getDefaultLocation(),
            userId: this.currentUserService.userId
        };

        return this.getDataSourceSource(apiUrl, requestParams);
    }

    getResponsibilities(filters: IDataSourceFilter) {
        filters = filters || {};
        let apiUrl = this.apiUrlsService.getRetriveSourceUrl() + "/getJobResponsibilities";
        let requestParams: Object;

        requestParams = {
            jobTitle: filters.JobTitle || null,
            employee: filters.employee || null
        };

        return this.getDataSourceSource(apiUrl, requestParams);
    }

    getSites(filters: IDataSourceFilter) {
        filters = filters || {};
        let apiUrl = this.apiUrlsService.getRetriveSourceUrl() + "/getSites";
        let requestParams = {
            jobTitle: filters.JobTitle || null,
            employee: filters.employee || null
        };

        return this.getDataSourceSource(apiUrl, requestParams);
    }

    getEmployees(filters: IDataSourceFilter) {
        filters = filters || {};
        let apiUrl = this.apiUrlsService.getRetriveSourceUrl() + "/getFilterdEmployees";
        let requestParams: Object;

        requestParams = {
            location: filters.location || this.currentUserService.getDefaultLocation(),
            Rating: filters.Rating || null,
            JobTitle: filters.JobTitle || null,
            date: filters.date || null,
            starttime: filters.starttime || null,
            endtime: filters.endtime || null,
            userId: this.currentUserService.userId,
            responsibility: filters.responsibility || null
        };

        return this.getDataSourceSource(apiUrl, requestParams);
    }

    private getDataSourceSource(apiUrl, requestParams) {

        let body: string;
        let options: RequestOptionsArgs;

        body = JSON.stringify(requestParams);
        options = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };


        return this.http
            .post(apiUrl, body, options)
            .map((response) => {
                var data = response.json().d;
                return data;
            })
            .catch(this.handleErrors);;
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleErrors(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}


export interface IDataSourceFilter {
    location?: string | R365GUID;
    employee?: string | R365GUID;
    Rating?: number;
    JobTitle?: string | R365GUID;
    date?: string | R365GUID;
    starttime?: string | R365GUID;
    endtime?: string | R365GUID;
    responsibility?: string | R365GUID;
}