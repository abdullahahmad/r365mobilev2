import {ENV} from './';

describe('testing env ',()=>{

    let env = new ENV();

    it('should be defined',()=>{
        expect(env).toBeDefined();
        
    });

    it("should expose dev & pro & isfunction",()=>{
        expect(env.dev).toBeDefined();
        expect(env.pro).toBeDefined();
        expect(env.is).toBeDefined();
        expect(env.selectedEnv).toBeDefined();
    });

    describe('testing production end ',()=>{
        
        it('is : should confirm using pro if pro env is selected ',()=>{
            env.selectedEnv = 'pro';
            expect(env.is('pro')).toBe(true);
            expect(env.getSelectedEnv()).toEqual(env.pro);
        });

        it('should point to the following params ',()=>{
            expect(env.pro.ApiEndPoint ).toEqual('https://{{hostName}}.restaurant365.net/');
            expect(env.pro.ServiceStackUrl).toEqual('ServiceStack');
            expect(env.pro.SQLProcUrl ).toEqual('SQLProc');
        });

        it('should falsify pro enviroment if the selected one is dev',()=>{
            env.selectedEnv = 'dev';
            expect(env.is('pro')).toBe(false);
            expect(env.getSelectedEnv()).not.toEqual(env.pro);
        });
    });
    
    describe('testing dev env',()=>{
        
        it('is : should confirm using dev if dev env is selected ',()=>{
            env.selectedEnv = 'dev';
            expect(env.is('dev')).toBe(true);
            expect(env.getSelectedEnv()).toEqual(env.dev);
        });

        it('should point to the following params ',()=>{
            expect(env.dev.ApiEndPoint ).toBeDefined();

            expect(env.dev.ServiceStackUrl).toBeDefined();
            expect(env.dev.ServiceStackPort).toBeDefined();
            
            expect(env.dev.SQLProcUrl ).toBeDefined();
            expect(env.dev.SQLProcPort).toBeDefined();

        });

        it('should falsify dev enviroment if the selected one is pro',()=>{
            env.selectedEnv = 'pro';
            expect(env.is('dev')).toBe(false);
            expect(env.getSelectedEnv()).not.toEqual(env.dev);
        });
    });
});