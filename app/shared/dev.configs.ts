import {Injectable} from '@angular/core';

@Injectable()
export class ENV {
    selectedEnv = 'dev';
    pro = {
        ApiEndPoint: "https://{{hostName}}.restaurant365.net/",
        ServiceStackUrl: "ServiceStack",
        SQLProcUrl: "SQLProc",
    };

    dev = {
        ApiEndPoint: "https://dev.restaurant365.net/dev/",
        ServiceStackPort: "ServiceStack",
        SQLProcPort: "SQLProc",
        ServiceStackUrl: ":54872",
        SQLProcUrl: ":55485",
        hostName: "dev"
    };
    
    public is(param) {
        return (this.selectedEnv == param);
    }
    public getSelectedEnv(){
        if(this.selectedEnv == 'pro'){
            return this.pro;
        }else{
            return this.dev;
        }
    }
}