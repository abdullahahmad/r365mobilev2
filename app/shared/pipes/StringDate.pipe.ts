import { Pipe, PipeTransform, Injectable } from '@angular/core';
import {DatePipe} from '@angular/common';
import * as moment from 'moment';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | stringDate:exponent
 * Example:
 *   {{ 2 |  stringDate:10}}
 *   formats to: 1024
*/
@Pipe({ name: 'stringDate' })
export class StringDatePipe implements PipeTransform {

    transform(value: string, orgFormat:string, dateFormat: string,isTime:boolean): string {
        
        let dateVal = moment(value,orgFormat).format();
        let result = ""; 
        
        if (!dateVal || dateVal.toString() == 'Invalid Date') {
            return result;
        }

        if(!dateFormat){
            dateFormat =  "h:mma";
        }
        result = moment(dateVal).format(dateFormat);
        return result.replace(/[Mm]/,"").replace(/:00/,"");
    }
}
