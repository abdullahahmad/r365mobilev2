import {Component, OnInit} from '@angular/core';
import {NavParams, Nav, ViewController} from 'ionic-angular';

/*
  Generated class for the AddressModal component.

  See https://angular.io/docs/ts/latest/api/core/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'address-modal',
  templateUrl: 'build/components/address-modal/address-modal.html'
})
export class AddressModal implements OnInit {
  address: IAddress;
  orgAddress: IAddress;
  constructor(private params: NavParams, private viewCtrl: ViewController) {

    this.orgAddress = Object.assign({}, params.get("address"));
    
    this.address =  Object.assign({}, params.get("address"));
  }

  ngOnInit() {
    console.log(this.params.data);
  }

  saveAddress() {
    this.viewCtrl.dismiss(this.address);
  }

  closeModal() {
    this.viewCtrl.dismiss(this.orgAddress);
  }
}

interface IAddress {
  Address1: string;
  Address2: string;
  City: string;
  State: string;
  Zip: string;
  Country: string;
}