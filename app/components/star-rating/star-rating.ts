import {Component, Input, Output, EventEmitter,OnChanges , OnInit} from '@angular/core';
import {Icon,Item} from 'ionic-angular';
/*
  Generated class for the StarRating component.

  See https://angular.io/docs/ts/latest/api/core/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'star-rating',
  templateUrl: 'build/components/star-rating/star-rating.html',
  directives:[Icon,Item]
})
export class StarRating implements OnChanges{

  ratingArr: number[] = [];
  ratingComplementArr: number[] = [];

  @Input() maxRating: number = 5;
  @Input() rating: number = 0;

  @Output() onChange = new EventEmitter<any>();;

  constructor() {
    this.rating = 0;
    this.maxRating  = 5; 
    this.ratingComplementArr = new Array(this.maxRating);
  }

  updateRating(index, isAbsolute) {
    this.rating = isNaN(+this.rating)?0:this.rating;
    if (this.rating == index && index == 1) {
      this.rating = 0;
    }
    else if (isAbsolute) {
      this.rating = index;
    } else {
      this.rating = this.rating + index;
    }
    this.ratingArr = new Array(this.rating);
    
    this.ratingComplementArr = new Array(Math.abs(5 - this.rating));
    this.onChange.emit(this.rating);
  }

  ngOnChanges(changes) {
      console.log(changes);
  }
}
